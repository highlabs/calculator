function moneyMask (selector) {
  VMasker(document.querySelector(selector)).maskMoney({
    unit: 'R$',
  })
}

function getPercent (value, percentage) {
  return (value/100) * percentage
}

function calculateTotal (value, mdr) {
  return value - getPercent(value, mdr)
}

function dailyMDR (value, days) {
  return value / days
}

function allPayments (total, payments, mdr) {
  const paymentsValue = total / payments
  let paymentsList = []

  for (let index = 0; index < payments; index++) {
    const totalPercentage = (index + 1) * mdr
    const discountedPayment = paymentsValue - getPercent(paymentsValue, totalPercentage)
    const paymentObj = {
      total: paymentsValue,
      daily: paymentsValue / 30,
      withMDR: discountedPayment,
      dailyMDR: dailyMDR(discountedPayment, 30)
    }
    paymentsList.push(paymentObj)
  }
  return paymentsList
}

function mdrListener () {
  const mdrInput = document.querySelector('#mdr')

  mdrInput.addEventListener('input', generateValues)
}

function formatValueToHTML (value) {
  return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(value)
}

function tomorrowCalc (payments) {
  let paymentTotal = 0
  payments.forEach(payment => {
    paymentTotal = paymentTotal + payment.dailyMDR * 30
  })
  return parseFloat(paymentTotal.toFixed(2))
}

function fifteenCalc () {
  return false
}

function thirtyCalc (payments) {
  let paymentTotal = 0

  payments.forEach((payment, index) => {
    if (index > 1) paymentTotal = paymentTotal + payment.daily * 30
    else paymentTotal = paymentTotal + payment.dailyMDR * 30
  })

  return parseFloat(paymentTotal.toFixed(2))
}

function generateValues (e) {
  const mdr = parseFloat(e.target.value)

  const payments = parseFloat(document.querySelector('#payments').value)
  const maskedValue = document.querySelector('#value')
  const value = parseFloat(maskedValue.value.replace('R$', '').replace(',', '.'))
  const total = calculateTotal(value, mdr)

  const paymentsList = allPayments(total, payments, mdr)


  const valueTomorrow = document.querySelector('.value-tomorrow')
  const value30days = document.querySelector('.value-30days')
  const value90days = document.querySelector('.value-90days')

  valueTomorrow.innerHTML = formatValueToHTML(tomorrowCalc(paymentsList))
  value30days.innerHTML = formatValueToHTML(thirtyCalc(30, paymentsList))
  value90days.innerHTML = formatValueToHTML(total)
}

document.addEventListener("DOMContentLoaded", function() {
  moneyMask('#value')
  mdrListener()
});

export {
  moneyMask,
  getPercent,
  calculateTotal,
  dailyMDR,
  allPayments,
  formatValueToHTML,
  tomorrowCalc,
  fifteenCalc,
  thirtyCalc,
  generateValues
};