const { getPercent, calculateTotal, dailyMDR, allPayments, tomorrowCalc, thirtyCalc, fifteenCalc } = require('./script')
let mockArray
describe('Simple calculations', () => {

  test('Get Percentage', () => {
    const percentage = getPercent(130, 4)
    expect(percentage).toBe(5.2)
  })

  test('Calculate total minus percentage', () => {
    const total = calculateTotal(150, 4)

    expect(total).toBe(144)
  })

  test('Calculate daily values', () => {
    const daily = dailyMDR(48, 30)

    expect(daily).toBe(1.6)
  })

  test('Calculate daily values', () => {
    const daily = dailyMDR(48, 30)

    expect(daily).toBe(1.6)
  })
})

describe('Create payments infos', () => {
  beforeAll(() => {
    mockArray = [
      {
        daily: 1.6,
        dailyMDR: 1.536,
        total: 48,
        withMDR: 46.08,
      },
      {
        daily: 1.6,
        dailyMDR: 1.472,
        total: 48,
        withMDR: 44.16,
      },
      {
        daily: 1.6,
        dailyMDR: 1.4080000000000001,
        total: 48,
        withMDR: 42.24,
      },
    ]

  })
  test('Create array os payment object', () => {
    const createdPaymentList = allPayments(144, 3, 4)

    expect(createdPaymentList).toEqual(mockArray)
  })

  describe('Calculate discounts values per day', () => {
    it('Tomorrow', () => {
      const total = tomorrowCalc(mockArray)

      expect(total).toBe(132.48)
    })

    it('15 days', () => {
      const total = fifteenCalc(mockArray)

      expect(total).toBe(133.44)
    })

    it('30 days', () => {
      const total = thirtyCalc(mockArray)

      expect(total).toBe(138.24)
    })

  })
})
